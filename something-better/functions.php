<?php
function somethingbetter_customize_register($wp_customize) {
	

	$wp_customize->add_section( 'somethingbetter' , array(
    		'title'      => __( 'Podtytul', 'somethingbetter' ),
    		'priority'   => 30,
		)
	);

	$wp_customize->add_setting( 'header_text_color' , array(
			'default'   => '#000000',
    		'transport' => 'postMessage',
		)
	);	

	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'link_color', array(
			'label'      => __( 'Kolor podtytulu', 'somethingbetter' ),
			'section'    => 'somethingbetter',
			'settings'   => 'header_text_color',
			)
		)
	);

		$wp_customize->add_setting( 'subtitle' , array(
			'default'   => 'Podaj drugi tytul',
    		'transport' => 'postMessage',
    		
		)
	);
			$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'subtitle', array(
			'label'      => __( 'Drugi tytul', 'somethingbetter' ),
			'section'    => 'somethingbetter',
			'settings'   => 'subtitle',
			'type'		 => 'text',
			)
		)
	);




	function somethingbetter_customize_css()
	{
    	?>
        	 <style type="text/css">
        	     h1 { color: <?php echo get_theme_mod('header_color', '#000000'); ?>; }
       		 </style>
    	<?php
	}
	add_action( 'wp_head', 'somethingbetter_customize_css');

 



}
add_action( 'customize_register', 'somethingbetter_customize_register' );

// function somethingbetter_register_my_setting() {
//     register_setting( 'general', 'second_subtitle' ); 
// } 
// add_action( 'admin_init', 'somethingbetter_register_my_setting' );